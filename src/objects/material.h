#ifndef MATERIAL_H
#define MATERIAL_H
#include <GL/glew.h>
#include <glm/glm.hpp>

class Material {
    public: 
    Material();
    Material(GLfloat pSpecularIntensity, GLfloat pShininess);
    void UseMaterial(GLuint pSpecularLocation, GLuint pShininessLocation);
    
    ~Material();

    protected:
    GLfloat aSpecularIntensity;
    GLfloat aShininess;


};
#endif