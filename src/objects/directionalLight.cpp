#include "directionalLight.h"
#include "light.h"
#include "../common.h"
DirectionalLight::DirectionalLight() : Light(){
    aDirection = -Y_DIR;
}

    // aDirection = pDirection;
DirectionalLight::DirectionalLight(
    GLfloat pShadowWidth, 
    GLfloat pShadowHeight, 
    vec3 pColor, 
    GLfloat pAmbienceIntensity, 
    vec3 pDirection, 
    GLfloat pDiffuseIntensity) 
: Light(
    pShadowWidth, 
    pShadowHeight, 
    pColor, 
    pAmbienceIntensity, 
    pDiffuseIntensity) {

    aDirection = pDirection;
    aLightProj = ortho(-GRID_SIZE/2.f, GRID_SIZE/2.f, -GRID_SIZE/2.f, GRID_SIZE/2.f, 0.1f, 200.f);
}

mat4 DirectionalLight::CalculateLightTransform() {
    return aLightProj * lookAt(-aDirection, vec3(0.f), Y_DIR);
}

void DirectionalLight::UseLight(
    GLuint pAmbientIntensityLocation, 
    GLuint pAmbientColourLocation, 
    GLuint pDiffuseIntensityLocation, 
    GLuint pDirectionLocation){
    Light::UseLight(pAmbientIntensityLocation, pAmbientColourLocation, pDiffuseIntensityLocation);
    glUniform3fv(pDirectionLocation, 1, &aDirection[0]);
        
}

DirectionalLight::~DirectionalLight(){}
