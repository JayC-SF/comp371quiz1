#ifndef LETTER_H
#define LETTER_H
#include "../common.h"
#include "../shaders/include.h"
#include "../texture.h"

class Letter {
    public:
    Letter(vec3 pColor);
    void Draw(Shader * shader);
    void SetBaseGroupMatrix(mat4 pBaseGroupMatrix);
    void SetColor(vec3 pColor);
    vec3 GetColor();
    vec2 GetWidthDepth();
    void Translate(vec3 pTranslate);
    void Update();
    protected:
    GLuint aEBOLength;
    vec2 aWidthDepth;
    vec3 aColor;
    mat4 aBaseGroupMatrix;
    mat4 aTranslationMatrix;
    mat4 aFinalGroupMatrix;
    vector<mat4> matrices;
    vector<vec3> aDims;
    Texture * aTexture;
    bool aIsUpdated;
};

class LetterS: public Letter{
    public:
    LetterS(vec3 pColor);
    private:
};

class LetterR: public Letter {
    public:
    LetterR(vec3 pColor);

};
class LetterE: public Letter {
    public:
    LetterE(vec3 pColor);

};
class LetterN: public Letter {
    public:
    LetterN(vec3 pColor);

};
#endif