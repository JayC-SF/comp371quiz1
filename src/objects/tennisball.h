#ifndef TENNIS_BALL_H
#define TENNIS_BALL_H
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../shaders/shader.h"

using namespace glm;
class TennisBall {
    public:
    TennisBall();
    TennisBall(GLfloat pRadius);

    void Draw(Shader * shader);
    void Translate(vec3 pTranslate);
    void aRotate(GLfloat aRads, vec3 pAxis);
    void Update();
    void SetRadius(GLfloat pRadius);

    protected:
    GLboolean aIsUpdated;
    GLfloat aRadius;
    mat4 aModelMatrix;
    mat4 aScale;
    mat4 aRotation;
    mat4 aTranslation;
};
#endif