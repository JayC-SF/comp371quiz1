#ifndef SKY_H
#define SKY_H
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include "../shaders/include.h"

using namespace glm;

class Sky {
    public:
    Sky(GLfloat pSize, vec3 pColor);
    void Draw(Shader * shader);
    protected:
    mat4 aModelMatrix;
    vec3 aColor; 
    GLfloat aSize;
};
#endif