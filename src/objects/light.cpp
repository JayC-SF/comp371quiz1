#include "light.h"
#include "../common.h"
Light::Light() {
    aColor = vec3(1.f);
    aAmbientIntensity = 1.f;
    aDiffuseIntensity = 0.f;
}
Light::Light(GLfloat pShadowWidth, GLfloat pShadowHeight, vec3 pColor, GLfloat pAmbienceIntensity, GLfloat pDiffuseIntensity) {
    aShadowMap = new ShadowMap();
    aShadowMap->Init(pShadowWidth, pShadowHeight);
    aColor = pColor;
    aAmbientIntensity = pAmbienceIntensity;
    aDiffuseIntensity = pDiffuseIntensity;
}

void Light::UseLight(GLuint pAmbientIntensityLocation, GLuint pAmbientColourLocation, 
    GLuint pDiffuseIntensityLocation) {
    glUniform1f(pAmbientIntensityLocation, aAmbientIntensity);
    glUniform3fv(pAmbientColourLocation, 1, &aColor[0]);
    
    glUniform1f(pDiffuseIntensityLocation, aDiffuseIntensity);

}

Light::~Light() {
    aColor = vec3(1.f);
}
