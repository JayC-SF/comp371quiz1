#ifndef LIGHT_H
#define LIGHT_H
#include "glm/glm.hpp"
#include "GL/glew.h"
#include "../ShadowMap.h"

using namespace glm;
using namespace std;

class Light {
    public: 
    Light();
    Light(GLfloat pShadowWidth, GLfloat pShadowHeight, vec3 pColor, GLfloat pAmbienceIntensity, GLfloat pDiffuseIntensity);
    ShadowMap * GetShadowMap() {return aShadowMap;}
    virtual void UseLight(GLuint pAmbientIntensityLocation, GLuint pAmbientColourLocation, 
        GLuint pDiffuseIntensityLocation);
    
    ~Light();

    protected:
    vec3 aColor;
    GLfloat aAmbientIntensity;
    GLfloat aDiffuseIntensity;
    mat4 aLightProj;
    ShadowMap * aShadowMap;
};
#endif