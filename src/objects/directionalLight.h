#ifndef DIRECTIONAL_LIGHT_H
#define DIRECTIONAL_LIGHT_H
#include "light.h"
class DirectionalLight : public Light {
    public:
    DirectionalLight();
    DirectionalLight(GLfloat pShadowWidth, GLfloat pShadowHeight, vec3 pColor, GLfloat pAmbienceIntensity, vec3 pDirection, GLfloat pDiffuseIntensity);
    
    void UseLight(GLuint pAmbientIntensityLocation, GLuint pAmbientColourLocation, 
        GLuint pDiffuseIntensityLocation, GLuint pDirectionLocation);

    mat4 CalculateLightTransform();

    ~DirectionalLight();

    protected:
    vec3 aDirection;


};
#endif