#include "net.h"
#include "atoms/cube.h"
#include "../texture.h"
TennisNet::TennisNet(vec3 pColor) {
    aColor = pColor;
    mat4 cubeOnGround = translate(mat4(1.0), 0.5f*U*Y_DIR);
    GLfloat stringThickness = 0.05*U;
    GLfloat spacing = 0.3;
    vec3 verticalString(stringThickness, 4*U, stringThickness);
    vec3 horizontalString(GRID_WIDTH*U, stringThickness, stringThickness);
    mat4 verticalStringM = scale(mat4(1.0), verticalString) * cubeOnGround;
    mat4 horizontalStringM = scale(mat4(1.0), horizontalString) * cubeOnGround;
    GLfloat xPos = 0; 
    // vertical strings
    for (; xPos <= (GRID_WIDTH)/2.f; xPos += stringThickness + spacing) {
        aMatrices.push_back(translate(mat4(1.0), xPos*U*X_DIR) * verticalStringM);
        aMatrices.push_back(translate(mat4(1.0), -xPos*U*X_DIR) * verticalStringM);
    }
    GLfloat yPos = 0;
    for(; yPos <= verticalString.y; yPos += stringThickness + spacing) {
        aMatrices.push_back(
            translate(mat4(1.0), yPos*Y_DIR) * horizontalStringM
        );
    }
    aWhiteBandDims = horizontalString;
    aWhiteBandDims.y = 6.f* aWhiteBandDims.y;
    whiteBand = translate(mat4(1.0f), (verticalString.y)*U*Y_DIR) *scale(mat4(1.f), aWhiteBandDims)* cubeOnGround;

    vec3 pillarDims(1*U, verticalString.y + 1*U, 1*U);
    vec3 middlePillarDims = pillarDims;
    middlePillarDims.y -= 1*U;
    mat4 sizedPillar = scale(mat4(1.0), pillarDims)*cubeOnGround;
    mat4 middlePillar = scale(mat4(1.0), middlePillarDims)*cubeOnGround;
    xPos = (GRID_WIDTH+ pillarDims.x)/2.f;
    pillars = {
        translate(mat4(1.f), xPos*X_DIR) * sizedPillar,
        translate(mat4(1.f), -xPos * X_DIR)* sizedPillar,
        middlePillar,
    };
}

void TennisNet::Draw(Shader * shader) {
    shader->UseShader();
    Cube * cube = Cube::GetInstance();
    cube->BindAttributes();
    shader->SetColor(vec3(aColor));
    for (int i = 0; i<aMatrices.size(); i++) {
        shader->SetModelMatrix(aMatrices[i]);
        cube->Draw();
    }
    shader->SetColor(vec3(192.f/255.f, 192.f/255.f, 192.f/255.f));
    bool isTextured = shader->GetIsTextured();
    shader->SetIsTextured(true);
    Texture * cementTexture = Texture::GetCementTexture();
    cementTexture->UseTexture();
    
    for (int i = 0; i<pillars.size(); i++) {
        shader->SetModelMatrix(pillars[i]);
        vec3 height = pillars[i] * vec4(Y_DIR/2.f, 1.f);
        cube->SetRepeatUVCoord(height.y, CUBE_FRONT_FACE, CUBE_Y_DIR);
        cube->SetRepeatUVCoord(height.y, CUBE_BACK_FACE, CUBE_Y_DIR);
        cube->SetRepeatUVCoord(height.y, CUBE_LEFT_FACE, CUBE_Y_DIR);
        cube->SetRepeatUVCoord(height.y, CUBE_RIGHT_FACE, CUBE_Y_DIR);
        cube->Draw();
    }
    // set back the uv max coords
    
    // set back the textured boolean to what it was
    
    // draw the white band
    shader->SetColor(vec3(1.f));
    shader->SetModelMatrix(whiteBand);
    Texture * tennisStrap = Texture::GetTennisStrapTexture();
    tennisStrap->UseTexture();
    
    cube->SetRepeatUVCoord(aWhiteBandDims.x, CUBE_FRONT_FACE, CUBE_X_DIR);
    cube->SetRepeatUVCoord(aWhiteBandDims.x, CUBE_BACK_FACE, CUBE_X_DIR);
    cube->SetRepeatUVCoord(aWhiteBandDims.x, CUBE_TOP_FACE, CUBE_X_DIR);
    cube->SetRepeatUVCoord(aWhiteBandDims.x, CUBE_BOTTOM_FACE, CUBE_X_DIR);
    cube->SetRepeatUVCoord(aWhiteBandDims.y, CUBE_FRONT_FACE, CUBE_Y_DIR);
    cube->SetRepeatUVCoord(aWhiteBandDims.y, CUBE_BACK_FACE, CUBE_Y_DIR);
    cube->SetRepeatUVCoord(aWhiteBandDims.z, CUBE_TOP_FACE, CUBE_Y_DIR);
    cube->SetRepeatUVCoord(aWhiteBandDims.z, CUBE_BOTTOM_FACE, CUBE_Y_DIR);

    cube->Draw();
    
    // restore values
    shader->SetIsTextured(isTextured);
    cube->ResetUVCoords();
    cube->UnbindAttributes();
}
