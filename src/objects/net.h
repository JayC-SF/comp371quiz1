#ifndef NET_H
#define NET_H
#include "../common.h"
#include "../shaders/include.h"
class TennisNet {
    public:
    TennisNet(vec3 pColor);
    void Draw(Shader * shader);
    protected:
    vector<mat4> aMatrices;
    vector<mat4> pillars;
    mat4 whiteBand;
    vec3 aColor;
    vec3 aWhiteBandDims;
};
#endif