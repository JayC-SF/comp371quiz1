#include "sky.h"
#include "../texture.h"
#include "atoms/cube.h"

Sky::Sky(GLfloat pSize, vec3 pColor) {
    aColor = pColor;
    aSize = pSize;
    aModelMatrix = scale(mat4(1.0), vec3(-pSize));
}

void Sky::Draw(Shader * shader) {
    shader->SetModelMatrix(aModelMatrix);
    bool isTextured = shader->GetIsTextured();
    shader->SetIsTextured(true);
    Texture * texture = Texture::GetSkyTexture();
    texture->UseTexture();
    Cube * cube = Cube::GetInstance();
    cube->ResetUVCoords();
    cube->BindAttributes();
    cube->Draw();
    cube->UnbindAttributes();
    shader->SetIsTextured(isTextured);
}