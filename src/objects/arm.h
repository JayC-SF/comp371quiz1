#ifndef ARM_H
#define ARM_H
#include <glm/glm.hpp>
#include <vector>
#include <GL/glew.h>
#include "racket.h"
#include "../shaders/shader.h"
using namespace glm;
using namespace std;

class Arm {
    public:
    Arm();
    void Draw(Shader * shader);
    void Translate(vec3 pTranslation);
    void Rotate(GLfloat aRads, vec3 pAxis);
    void Scale(GLfloat pScale);
    void RotateElbow(GLfloat aRads, vec3 pAxis);
    void RotateWrist(GLfloat aRads, vec3 pAxis);
    void ResetTranslation();
    void ResetRotation();
    void ResetScale();
    void Reset();
    void Update();
    Racket * GetRacket();
    vec3 GetCenterPosition();
    protected:
    // shader attributes
    // GLuint aShaderProgramId;
    // GLuint aModelMatrixLocation;
    // GLuint aColorLocation;
    
    // check if the group matrices are updated and ready for drawing.
    bool aIsUpdated;
    
    // model attributes
    GLfloat aTotalModelScale;
    mat4 aTranslation;
    mat4 aRotation;
    mat4 aElbowRotation;
    mat4 aWristRotation;
    mat4 aShoulderGroupMatrix;
    mat4 aBrachium;
    mat4 aElbowGroupMatrix;
    mat4 aDefaultElbowGroupMatrix;
    mat4 aForearm;
    mat4 aWristGroupMatrix;
    mat4 aDefaultWristGroupMatrix;
    vec3 aArmDims;
    Racket aRacket;

    private:
};
#endif