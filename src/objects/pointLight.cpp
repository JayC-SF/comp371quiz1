#include "pointLight.h"
#include "light.h"
PointLight::PointLight() : Light(){
    aPosition = vec3(1.f);
    aConstant = 1.f; 
    aLinear = 0; 
    aExponent = 0;
}


PointLight::PointLight(
    vec3 pColor, 
    GLfloat pIntensity, 
    GLfloat pDiffuseIntensity, 
    vec3 pPosition,
    GLfloat pConstant,
    GLfloat pLinear,
    GLfloat  pExponent) :Light(1024, 1024,  pColor, pIntensity, pDiffuseIntensity){
    
    aPosition = pPosition;
    aConstant = pConstant;
    aLinear = pLinear;
    aExponent = pExponent;

}

void PointLight::UseLight(
    GLuint pAmbientIntensityLocation, 
    GLuint pAmbientColourLocation, 
    GLuint pDiffuseIntensityLocation, 
    GLuint pPositionLocation,
    GLuint pConstantLocation, 
    GLuint pLinearLocation,
    GLuint pExponentLocation) {
    
    Light::UseLight(pAmbientIntensityLocation, pAmbientColourLocation, pDiffuseIntensityLocation);
    glUniform3fv(pPositionLocation, 1, &aPosition[0]);
    glUniform1f(pConstantLocation, aConstant);
    glUniform1f(pLinearLocation, aLinear);
    glUniform1f(pExponentLocation, aExponent);
}


PointLight::~PointLight(){}