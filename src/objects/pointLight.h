#ifndef POINT_LIGHT_H
#define POINT_LIGHT_H
#include "light.h"
class PointLight : public Light{ 
    public:
    PointLight();
    PointLight(vec3 pColor, GLfloat pIntensity, GLfloat pDiffuseIntensity, 
        vec3 pPosition, GLfloat pConstant, GLfloat pLinear,GLfloat  pExponent);

    void UseLight(GLuint pAmbientIntensityLocation, GLuint pAmbientColourLocation, 
        GLuint pDiffuseIntensityLocation, GLuint pPositionLocation,
        GLuint pConstantLocation, GLuint pLinearLocation,GLuint  pExponentLocation);

    ~PointLight();

    protected:
    GLfloat aConstant, aLinear, aExponent;
    vec3 aPosition;
};
#endif