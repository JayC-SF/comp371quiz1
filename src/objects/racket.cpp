#include "racket.h"
#include "../common.h"
#include "atoms/cube.h"
#include "../shaders/include.h"
#include "../texture.h"
// 
Racket::Racket() :
// define default initialized values;
// aShaderProgramId(pShaderProgramId),
// aModelMatrixLocation(pModelMatrixLocation), 
// aColorLocation(pColorLocation),
aTranslation(mat4(1.f)),
aRotation(mat4(1.f)), 
aBaseGroupMatrix(mat4(1.f)),
aTotalModelScale(1.f),
aColor(vec3(1.f, 0.f, 0.f)), 
aIsUpdated(false)
{
    vec3 racketBlockDims(0.15f*U, 1.f*U, 0.15f*U);
    mat4 cubeOnGround = translate(mat4(1.f), 0.5f * Y_DIR);

    // create a new block and add 0.3f to the height of the block to 
    vec3 racketHandleDims = racketBlockDims + vec3(0.f, .1f, 0.f);
    aRacketHandle = scale(mat4(1.f), racketHandleDims) * cubeOnGround;

    // scale the stick to be sitting on xz plane and length vertically.
    // create a bottom rim with racket handle dimensions. 
    // bottom face facing the ground
    // and x and z centered at origin. 
    GLfloat rimThickness = racketHandleDims.z*0.5;
    aRacketTopBotRimDims = vec3(racketHandleDims.y*0.9, rimThickness, rimThickness);
    mat4 shapeRacketBottomTopRim = scale(mat4(1.f), aRacketTopBotRimDims);
    mat4 transformRacketBottomRimGroup = translate(mat4(1.f), racketHandleDims.y * U * Y_DIR);
    aRacketBottomRim =  transformRacketBottomRimGroup * shapeRacketBottomTopRim * cubeOnGround;

    vec3 sideRimDims = vec3(rimThickness, racketHandleDims.y * 1.5f, rimThickness);
    mat4 scaledSideRim = scale(mat4(1.f), sideRimDims) * cubeOnGround;
    vec3 posXSideRimTranslation = (aRacketTopBotRimDims.x + sideRimDims.x)/2.f *U*X_DIR;
    // negative x translation.
    mat4 racketLeftRimGroup = translate(transformRacketBottomRimGroup, -posXSideRimTranslation);
    aRacketLeftRim = racketLeftRimGroup * scaledSideRim;
    
    // right racket rim.
    // positive x translation
    aRacketRightRim = translate(transformRacketBottomRimGroup, posXSideRimTranslation) * scaledSideRim;
    mat4 transformRacketTopRimGroup = transformRacketBottomRimGroup * translate(mat4(1.f), (sideRimDims.y - aRacketTopBotRimDims.y)*U*Y_DIR);
    aRacketTopRim = transformRacketTopRimGroup * shapeRacketBottomTopRim * cubeOnGround;

    GLfloat meshHeight = sideRimDims.y - 2 * aRacketTopBotRimDims.y;
    GLfloat meshWidth = aRacketTopBotRimDims.x;
    // make the spacing between strings 10% of the mesh width
    GLfloat spacingBetweenStrings = 0.10 * meshWidth;
    // make the mesh strings width to be half of the spacing between them.
    GLfloat stringWidth = 0.08 * spacingBetweenStrings;
    vec3 verticalStringDims(stringWidth, meshHeight, stringWidth);
    vec3 horizontalStringDims(meshWidth, stringWidth, stringWidth);
    
    // don't create first and last one on the horizontal axis
    vec3 stringPos(
        (-meshWidth/2 + spacingBetweenStrings)*U, // x
        aRacketTopBotRimDims.y*U,                  // y
        0.f);                                     // z;

    mat4 stringMatrix = scale(mat4(1.f), verticalStringDims) * cubeOnGround;
    for (; stringPos.x < meshWidth/2.f; stringPos.x += spacingBetweenStrings + verticalStringDims.x) {
        mat4 m = 
            transformRacketBottomRimGroup *
            translate(mat4(1.f), stringPos) *
            stringMatrix;
        aMeshMatrices.push_back(m);
    }
    stringPos.y = aRacketTopBotRimDims.y + spacingBetweenStrings;
    stringPos.x = 0;
    stringMatrix = scale(mat4(1.f), horizontalStringDims) * cubeOnGround;
    for (; stringPos.y < meshHeight + aRacketTopBotRimDims.y; stringPos.y += spacingBetweenStrings + horizontalStringDims.y) {
        mat4 m = 
            transformRacketBottomRimGroup *
            translate(mat4(1.f), stringPos) *
            stringMatrix;
        aMeshMatrices.push_back(m);
    }
    
    aLetterUpTranslate = transformRacketTopRimGroup * translate(mat4(1.0), ((float)U/3.f)*Y_DIR);

}

// 
void Racket::Draw(Shader * shader) {
    Update();
    Cube * cube = Cube::GetInstance();
    cube->BindAttributes();
    Texture * glossyTexture = Texture::GetSilverMetalTexture();
    glossyTexture->UseTexture();
    bool isTextured = shader->GetIsTextured();
    shader->SetColor(vec3(aColor));
    shader->SetModelMatrix(aWristMatrix * aRacketHandle);
    cube->SetRepeatUVCoord(aRacketTopBotRimDims);
    cube->Draw();
    
    // draw single diagonal on stick
    shader->SetColor(vec3(1.f));
    shader->SetIsTextured(true);
    shader->SetModelMatrix(aWristMatrix * aRacketBottomRim);

    cube->Draw();
    
    // draw left rim
    shader->SetColor(aColor);
    shader->SetIsTextured(false);
    shader->SetModelMatrix(aWristMatrix * aRacketLeftRim);
    cube->Draw();
    
    // draw right rim
    shader->SetModelMatrix(aWristMatrix * aRacketRightRim);
    cube->Draw();
    
    // draw top rim
    shader->SetColor(vec3(1.f));
    shader->SetIsTextured(true);
    shader->SetModelMatrix(aWristMatrix * aRacketTopRim);
    cube->Draw();
    shader->SetIsTextured(false);
    shader->SetColor(vec3(0.f, 1.f, 0.f));
    for (int i = 0; i<aMeshMatrices.size(); i++) {
        shader->SetModelMatrix(aWristMatrix * aMeshMatrices[i]);
        cube->Draw();
    }

    shader->SetIsTextured(isTextured);

    // back letter
    for (int i = 0; i<letters.size(); i++) {
        letters[i].SetBaseGroupMatrix(
            aWristMatrix * 
            translate(mat4(1.0), -2.f *letters[i].GetWidthDepth().y*Z_DIR) *
            aLetterUpTranslate);
        // keep track of original color
        vec3 letterColor = letters[i].GetColor();
        // draw with different shade
        letters[i].SetColor(0.2f*letterColor);
        letters[i].Draw(shader);
        // restore color
        letters[i].SetColor(letterColor);
    }

    // middle letter
    for (int i = 0; i<letters.size(); i++) {
        letters[i].SetBaseGroupMatrix(aWristMatrix * aLetterUpTranslate);
        // keep track of color
        vec3 letterColor = letters[i].GetColor();
        // draw with different shade
        letters[i].SetColor(0.7f*letterColor);
        letters[i].Draw(shader);
        // restore color
        letters[i].SetColor(letterColor);
    }
    
    // front letter
    for (int i = 0; i<letters.size(); i++) {
        letters[i].SetBaseGroupMatrix(
            aWristMatrix * 
            translate(mat4(1.0), 2.f*letters[i].GetWidthDepth().y*Z_DIR) *
            aLetterUpTranslate);
        letters[i].Draw(shader);
    }
    cube->UnbindAttributes();
}

// 
void Racket::Translate(vec3 pTranslation) {
    aTranslation = translate(mat4(1.f), pTranslation) * aTranslation;
    aIsUpdated = false;
}

// 
void Racket::Rotate(GLfloat pRads, vec3 pAxis) {
    aRotation = rotate(mat4(1.f), pRads, pAxis) * aRotation;
    aIsUpdated = false;
}

// 
void Racket::Scale(GLfloat pScale) {
    aTotalModelScale += pScale;
    aIsUpdated = false;
}

// 
void Racket::SetBaseGroupMatrix(mat4 pBaseGroupMatrix) {
    aBaseGroupMatrix = pBaseGroupMatrix;
    aIsUpdated = false;
}

// 
void Racket::SetColor(vec3 pColor) {
    aColor = pColor;
}

// 
void Racket::Update() {
    if(aIsUpdated)
        return;
    aWristMatrix = aBaseGroupMatrix * aTranslation * aRotation * scale(mat4(1.f), aTotalModelScale*vec3(1.f));
    aIsUpdated = true;
}

void Racket::AddLetter(Letter pLetter) {
    letters.push_back(pLetter);
}
