#include "ground.h"
#include <vector>
#include "../common.h"
#include "atoms/cube.h"
#include "../shaders/include.h"


Ground::Ground(GLuint pGridWidth, GLuint pGridLength, vec3 pColor)
{
    vector<vec3> vertices;
    vector<GLuint> indices;
    GLdouble halfGridWidth = pGridWidth/2.0f * U;
    GLdouble halfGridLength = pGridLength/2.0f * U;
    // start with corner.
    for (int i = 0; i<=pGridLength; i++) {
        // set a pair of vertices to form a line, placing one on positive x and other on negative x.
        // Building lines from the negative z axis to the positive axis.
        vertices.push_back(vec3(-halfGridWidth, 0.f, -halfGridLength + i*U));
        vertices.push_back(vec3(halfGridWidth, 0.f, -halfGridLength + i*U));
    }
    // start at 1 because pair of vertices are already setup from previous for loop.
    for (int i = 0; i<=pGridWidth; i++) {
        // set a pair of vertices to form a line, placing one on positive x and other on negative x.
        // Building lines from the negative z axis to the positive axis.
        vertices.push_back(vec3(-halfGridWidth + i*U, 0.f, -halfGridLength));
        vertices.push_back(vec3(-halfGridWidth + i*U, 0.f, halfGridLength));
    }
    for (int i = 0; i < vertices.size(); i++) {
        indices.push_back(i);
    }

    aVAO = VAO();
    aVAO.Bind();
    aVBO.Init(vertices.size()*sizeof(vec3), &vertices[0][0], GL_STATIC_DRAW);
    aVAO.VAttribPointer(aVBO, 0, 3, GL_FLOAT, sizeof(vec3), NULL);
    aEBO.Init(indices.size(), &indices[0], GL_STATIC_DRAW, GL_LINES);

    aColor = pColor;
    aGroundRotation = mat4(1.0);
    aModelMatrix = mat4(1.0);
    
    // ground for the cube.
    // translate the cube right below the xz plane
    // scale the cube by 100 on x and z axis
    aGroundMatrix = scale(mat4(1.0), vec3(pGridWidth, 1.f, pGridLength)) * translate(mat4(1.f), -1.f*Y_DIR);
}

void Ground::Draw(Shader * shader) {
    Cube * cube = Cube::GetInstance();
    shader->UseShader();
    // bind info in shader.
    shader->SetModelMatrix(aGroundMatrix);
    Texture * clayTexture = Texture::GetClayTexture();
    clayTexture->UseTexture();

    shader->SetColor(vec3(aColor));
    cube->BindAttributes();
    cube->Draw();
    
    shader->SetColor(vec3(1.f));
    shader->SetModelMatrix(aModelMatrix);
    aVAO.Bind();
    aEBO.Bind();
    aEBO.Draw();
    aEBO.Unbind();
    aVAO.Unbind();
    // unbind and cleanup
    cube->UnbindAttributes();
}