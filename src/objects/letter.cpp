#include "letter.h"
#include "atoms/cube.h"
Letter::Letter(vec3 pColor) {
    aColor = pColor;
    aBaseGroupMatrix = mat4(1.0);
    aWidthDepth = ((float)U)*vec2(0.2f, 0.2f);
    aEBOLength = 36;
    aTranslationMatrix = mat4(1.0);
    aIsUpdated = false;
}

void Letter::SetBaseGroupMatrix(mat4 pBaseGroupMatrix) {
    aBaseGroupMatrix = pBaseGroupMatrix;
    aIsUpdated = false;
}

void Letter::Draw(Shader * shader) {
    Update();
    Cube * cube = Cube::GetInstance();
    shader->SetColor(vec3(aColor));
    cube->BindAttributes();
    
    bool isTextured = shader->GetIsTextured();
    aTexture->UseTexture();
    for (int i = 0; i<matrices.size(); i++) {
        shader->SetModelMatrix(aFinalGroupMatrix * matrices[i]);
        shader->SetColor(vec4(aColor, 0.3));
        cube->SetRepeatUVCoord(aDims[i]);
        
        shader->SetIsTextured(false);

        cube->DrawFace(CUBE_FRONT_FACE);
        shader->SetIsTextured(true);
        shader->SetColor(vec4(aColor, 1.f));
        cube->DrawFace(CUBE_BACK_FACE);
        cube->DrawFace(CUBE_TOP_FACE);
        cube->DrawFace(CUBE_BOTTOM_FACE);
        cube->DrawFace(CUBE_RIGHT_FACE);
        cube->DrawFace(CUBE_LEFT_FACE);
    }
    shader->SetIsTextured(isTextured);
    cube->UnbindAttributes();
}

void Letter::SetColor(vec3 pColor){
    aColor = pColor;
}

vec2 Letter::GetWidthDepth() {
    return aWidthDepth;
}

LetterS::LetterS(vec3 pColor) : Letter(pColor){
    mat4 cubeOnGround = translate(mat4(1.0), 0.5f*U*Y_DIR);
    vec3  bottomTopMiddleRimDims = vec3(1.f*U, aWidthDepth.x, aWidthDepth.y);
    mat4 bottom = scale(mat4(1.0), bottomTopMiddleRimDims) * cubeOnGround;
    // add bottom to be processed.
    matrices.push_back(bottom);
    aDims.push_back(bottomTopMiddleRimDims);
    // 
    vec3 rightLeftRimDims = vec3(aWidthDepth.x, 0.5f*bottomTopMiddleRimDims.x, aWidthDepth.y);
    mat4 bottomRight(1.0);
    mat4 bottomRightGroup = translate(mat4(1.0), ((-rightLeftRimDims.x/2.f + bottomTopMiddleRimDims.x/2)*X_DIR + bottomTopMiddleRimDims.y*Y_DIR)*(float)U);
    bottomRight = bottomRightGroup * scale(mat4(1.0), rightLeftRimDims) * cubeOnGround;
    
    matrices.push_back(bottomRight);
    aDims.push_back(rightLeftRimDims);

    mat4 middle = scale(mat4(1.0), bottomTopMiddleRimDims) * cubeOnGround;
    mat4 middleGroup = bottomRightGroup *
        translate(mat4(1.0), -0.5f*(-rightLeftRimDims.x + bottomTopMiddleRimDims.x)*U*X_DIR) *
        translate(mat4(1.0), (rightLeftRimDims.y)*U*Y_DIR);
    middle = middleGroup * middle;
    
    matrices.push_back(middle);
    aDims.push_back(bottomTopMiddleRimDims);
    
    mat4 topLeftGroup = middleGroup *
        translate(mat4(1.0), (-(-rightLeftRimDims.x/2.f + bottomTopMiddleRimDims.x/2)*X_DIR + bottomTopMiddleRimDims.y*Y_DIR)*(float)U);
    mat4 topLeft = topLeftGroup * scale(mat4(1.0), rightLeftRimDims) * cubeOnGround;
    
    matrices.push_back(topLeft);
    aDims.push_back(rightLeftRimDims);

    mat4 top = scale(mat4(1.0), bottomTopMiddleRimDims) * cubeOnGround;
    mat4 topGroup =
        topLeftGroup *
        translate(mat4(1.0), 0.5f*(bottomTopMiddleRimDims.x - rightLeftRimDims.x)*U*X_DIR + (rightLeftRimDims.y)*Y_DIR);
        top = topGroup * top;
    
    matrices.push_back(top);
    aDims.push_back(bottomTopMiddleRimDims);
    
    aTexture = Texture::GetTennisBallTexture();
}

LetterR::LetterR(vec3 pColor) : Letter(pColor){
    mat4 cubeOnGround = translate(mat4(1.0), 0.5f*U*Y_DIR);
    vec3 topRimDims = vec3(1.f*U, aWidthDepth.x, aWidthDepth.y);
    vec3 leftRimDims = vec3(aWidthDepth.x, topRimDims.x, aWidthDepth.y);
    mat4 leftRimGroup = 
        translate(mat4(1.0), (-topRimDims.x/2.f*U)*X_DIR);
    mat4 leftRim = leftRimGroup *
        scale(mat4(1.0), leftRimDims) *
        cubeOnGround;
    
    matrices.push_back(leftRim);
    aDims.push_back(leftRimDims);

    vec3 middleRimDims = vec3(topRimDims.x - 2.f*leftRimDims.x, topRimDims.y, topRimDims.z);
    mat4 middleRimGroup = leftRimGroup *
        translate(mat4(1.0), vec3(middleRimDims.x/2.f*U, leftRimDims.y/2.f, 0));
    mat4 middleRim = 
        middleRimGroup *
        scale(mat4(1.f), middleRimDims);
        
    matrices.push_back(middleRim);
    aDims.push_back(middleRimDims);
    
    mat4 topRimGroup = middleRimGroup *
        translate(mat4(1.0), vec3(0, leftRimDims.y/2.f - middleRimDims.y/2.f, 0));
    mat4 topRim = 
        topRimGroup *
        scale(mat4(1.f), middleRimDims);
    
    matrices.push_back(topRim);
    aDims.push_back(middleRimDims);

    vec3 rightRimDims(leftRimDims.x, leftRimDims.y/2.f + middleRimDims.y/2.f, leftRimDims.z);
    mat4 rightRimGroup = middleRimGroup * translate(mat4(1.0), vec3(middleRimDims.x/2.f*U, -middleRimDims.y/2.f*U, 0));
    mat4 rightRim = rightRimGroup * scale(mat4(1.0), rightRimDims) * cubeOnGround;
    
    matrices.push_back(rightRim);
    aDims.push_back(middleRimDims);

    vec3 bottomRightRimDims(leftRimDims.x, leftRimDims.y/2.f - middleRimDims.y/2.f, leftRimDims.z);
    mat4 bottomRightRim = 
        middleRimGroup *
        translate(mat4(1.0), vec3(middleRimDims.x/2.f - bottomRightRimDims.x/2.f, -middleRimDims.y/2.f, 0)) *
        scale(mat4(1.0), bottomRightRimDims) * translate(mat4(1.0), -0.5f*U*Y_DIR);
    
    matrices.push_back(bottomRightRim);
    aDims.push_back(bottomRightRimDims);
    
    aTexture = Texture::GetTattooTexture();
}

LetterE::LetterE(vec3 pColor) : Letter(pColor){
    mat4 cubeOnGround = translate(mat4(1.0), 0.5f*U*Y_DIR);
    vec3 leftRimDims = vec3(aWidthDepth.x, 1.f*U, aWidthDepth.y);
    vec3 threeHorizontalRimDims(leftRimDims.y-leftRimDims.x, leftRimDims.x, leftRimDims.z);
    mat4 leftRimGroup =
        translate(mat4(1.0), -(leftRimDims.y/2.f - leftRimDims.x/2.f)*X_DIR);
    mat4 leftRim = leftRimGroup * scale(mat4(1.0), leftRimDims) * cubeOnGround;
    
    matrices.push_back(leftRim);
    aDims.push_back(leftRimDims);

    mat4 bottomRim = leftRimGroup * 
        translate(mat4(1.0), threeHorizontalRimDims.x/2.f*U*X_DIR) *
        scale(mat4(1.0),threeHorizontalRimDims)*cubeOnGround;
    
    matrices.push_back(bottomRim);
    aDims.push_back(threeHorizontalRimDims);

    mat4 middleRim = leftRimGroup *
        translate(mat4(1.0), leftRimDims.y/2.f*Y_DIR) *
        translate(mat4(1.0), threeHorizontalRimDims.x/2.f*U*X_DIR) *
        scale(mat4(1.0),threeHorizontalRimDims);
    
    matrices.push_back(middleRim);
    aDims.push_back(threeHorizontalRimDims);

    mat4 topRim = leftRimGroup * 
        translate(mat4(1.0), leftRimDims.y*Y_DIR) *
        translate(mat4(1.0), threeHorizontalRimDims.x/2.f*U*X_DIR) *
        scale(mat4(1.0),threeHorizontalRimDims)*
        translate(mat4(1.0), -0.5f*U*Y_DIR);
    
    matrices.push_back(topRim);
    aDims.push_back(threeHorizontalRimDims);

    aTexture = Texture::GetWoodTexture();
}

LetterN::LetterN(vec3 pColor) : Letter(pColor){
    mat4 cubeOnGround = translate(mat4(1.0), 0.5f*U*Y_DIR);
    vec3 verticalRimDims = vec3(aWidthDepth.x, 1.f*U, aWidthDepth.y);
    vec3 blocks(verticalRimDims.x, verticalRimDims.x, verticalRimDims.z);
    mat4 leftRimGroup = translate(mat4(1.0), -(verticalRimDims.y/2.f - verticalRimDims.x/2.f)*X_DIR);
    mat4 leftRim = leftRimGroup * scale(mat4(1.0), verticalRimDims) * cubeOnGround;
    
    matrices.push_back(leftRim);
    aDims.push_back(verticalRimDims);

    mat4 middleBlockGroup =
        translate(mat4(1.0), verticalRimDims.y/2.f*U*Y_DIR);
    mat4 middleBlock = middleBlockGroup * scale(mat4(1.0), blocks);
    
    matrices.push_back(middleBlock);
    aDims.push_back(blocks);
    
    mat4 topBlock = 
        middleBlockGroup *
        translate(mat4(1.0), blocks.y*U*Y_DIR) *
        translate(mat4(1.0), -blocks.y*U*X_DIR) *
        scale(mat4(1.0), blocks);
    
    matrices.push_back(topBlock);
    aDims.push_back(blocks);
    
    mat4 bottomBlock =
        middleBlockGroup *
        translate(mat4(1.0), -blocks.y*U*Y_DIR) *
        translate(mat4(1.0), blocks.y*U*X_DIR) *
        scale(mat4(1.0), blocks);
    
    matrices.push_back(bottomBlock);
    aDims.push_back(blocks);

    mat4 rightRimGroup = translate(mat4(1.0), (verticalRimDims.y/2.f - verticalRimDims.x/2.f)*X_DIR);
    mat4 rightRim = rightRimGroup * scale(mat4(1.0), verticalRimDims) * cubeOnGround;
    
    matrices.push_back(rightRim);
    aDims.push_back(verticalRimDims);

    aTexture = Texture::GetCementTexture();
}

void Letter::Translate(vec3 pTranslate) {
    aTranslationMatrix = translate(mat4(1.0), pTranslate) * aTranslationMatrix;
    aIsUpdated = false;
}

void Letter::Update() {
    if (aIsUpdated)
        return;
    aFinalGroupMatrix = aBaseGroupMatrix * aTranslationMatrix;
    aIsUpdated = true;
}

vec3 Letter::GetColor() {
    return aColor;
}