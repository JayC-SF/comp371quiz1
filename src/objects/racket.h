#ifndef RACKET_H
#define RACKET_H
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <vector>
#include "../shaders/shader.h"
#include "letter.h"
using namespace glm;
using namespace std;
class Racket {
    public:
    Racket();
    void Draw(Shader * shader);
    void Translate(vec3 pTranslation);
    void Rotate(GLfloat pRads, vec3 pAxis);
    void Scale(GLfloat);
    void SetBaseGroupMatrix(mat4 pBaseGroupMatrix);
    void SetColor(vec3 pColor);
    void AddLetter(Letter pLetter);
    void Update();
    protected:

    // translation and rotation of the racket.
    mat4 aTranslation;
    mat4 aRotation;
    // defines the scale of the model.
    GLfloat aTotalModelScale;
    // base matrix dependent on other object. This matrix will place 
    // the racket on the other dependend object
    mat4 aBaseGroupMatrix;
    // wrist matrix is the base matrix of the racket.
    mat4 aWristMatrix;

    // check if attributes are updated and ready to draw.
    bool aIsUpdated;

    // color
    vec3 aColor;
    // matrices for drawing the racket.
    mat4 aRacketHandle;
    mat4 aRacketBottomRim;
    mat4 aRacketLeftRim;
    mat4 aRacketRightRim;
    mat4 aRacketTopRim;
    mat4 aLetterUpTranslate;
    vec3 aRacketTopBotRimDims;
    vector<mat4> aMeshMatrices;
    vector<Letter> letters;
    private:
};
#endif