#include "arm.h"
#include "../common.h"
#include "atoms/include.h"
#include "../shaders/include.h"
#include "../texture.h"

// 
Arm::Arm() :
    // default values for the following attributes
    // aShaderProgramId(pShaderProgramId),
    // aModelMatrixLocation(pModelMatrixLocation),
    // aColorLocation(pColorLocation),
    aTranslation(mat4(1.0)), 
    aRotation(mat4(1.0)), 
    aElbowRotation(mat4(1.0)),
    aWristRotation(mat4(1.0)),
    aTotalModelScale(1.f), 
    aIsUpdated(false)
{
    aArmDims = vec3(0.3f*U, 1.f*U, 0.3f*U);
    mat4 cubeOnGround = translate(mat4(1.0f), 0.5f * Y_DIR);
    
    mat4 armSample = scale(mat4(1.0), aArmDims) * cubeOnGround;

    aBrachium = armSample;
    aForearm = armSample;

    // have the brachium and more to be rotated by 30 degrees.
    aRotation = rotate(mat4(1.0), -radians<float>(30), Z_DIR) * aRotation;
    aShoulderGroupMatrix = aTranslation * aRotation;
    // translate slightly to the right to prepare for rotation.
    mat4 elbowTranslate1 = translate(mat4(1.0), vec3(-aArmDims.x/2, .0f, 0.f));
    // rotate the arm on the edge of forearm
    mat4 elbowRotate = rotate(mat4(1.0), -radians<float>(30), vec3(0.f, 0.f, -1.f));
    // translate back and place at the tip of brachium.
    mat4 elbowTranslate2 = translate(mat4(1.0), vec3(aArmDims.x/2, aArmDims.y, 0.f));

    aDefaultElbowGroupMatrix = elbowTranslate2 * elbowRotate * elbowTranslate1;
    aElbowGroupMatrix = aDefaultElbowGroupMatrix;
    // when multipliying this matrix it will place the elbow ready for rotation.
    aDefaultWristGroupMatrix = translate(mat4(1.0), vec3(0.f, aArmDims.y, 0.f));
    aWristGroupMatrix = aDefaultWristGroupMatrix;
}

// 
void Arm::Draw(Shader * shader) {
    Update();
    shader->UseShader();
    Cube * cube = Cube::GetInstance();
    cube->BindAttributes();
    Texture * armTexture = Texture::GetTattooTexture();
    armTexture->UseTexture();
    bool isTextured = shader->GetIsTextured();
    shader->SetIsTextured(true);
    cube->ResetUVCoords();
    cube->SetRepeatUVCoord(aArmDims/3.f);
    // draw brachium
    shader->SetColor(vec3(200./255., 173./255., 127./255.));
    shader->SetModelMatrix(aShoulderGroupMatrix * aBrachium);
    cube->Draw();

    // draw forearm and use same color earlier
    shader->SetModelMatrix(aShoulderGroupMatrix * aElbowGroupMatrix * aForearm);
    cube->Draw();
    shader->SetIsTextured(isTextured);
    aRacket.Draw(shader);

}

// 
void Arm::Translate(vec3 pTranslation) {
    aTranslation = translate(mat4(1.0), pTranslation) * aTranslation;
    aIsUpdated = false;
}

// 
void Arm::Rotate(GLfloat aRads, vec3 pAxis) {
    aRotation = rotate(mat4(1.0), aRads, pAxis) * aRotation;
    aIsUpdated = false;
}

// totalScale
void Arm::Scale(GLfloat pScale) {
    aTotalModelScale += pScale;
    if (aTotalModelScale <= 0.f)
        aTotalModelScale = 0;
    aIsUpdated = false;
}
void Arm::RotateElbow(GLfloat aRads, vec3 pAxis) {
    aElbowRotation = rotate(mat4(1.0), aRads, pAxis) * aElbowRotation;
    aIsUpdated = false;
}
void Arm::RotateWrist(GLfloat aRads, vec3 pAxis) {
    aWristRotation = rotate(mat4(1.0), aRads, pAxis) * aWristRotation;
    aIsUpdated = false;
}
// 
void Arm::Update() {
    // return if updated
    if (aIsUpdated)
        return;
    aShoulderGroupMatrix = aTranslation * aRotation * scale(mat4(1.f), aTotalModelScale*vec3(1.f));
    aElbowGroupMatrix = aDefaultElbowGroupMatrix * aElbowRotation;
    aWristGroupMatrix = aDefaultWristGroupMatrix * aWristRotation;
    aRacket.SetBaseGroupMatrix(aShoulderGroupMatrix * aElbowGroupMatrix * aWristGroupMatrix);
    aIsUpdated = true;
}

// 
void Arm::ResetTranslation() {
    aTranslation = mat4(1.0);
    aIsUpdated = false;
}

// 
void Arm::ResetRotation() {
    aRotation = rotate(mat4(1.0), -radians<float>(30), Z_DIR);
    aIsUpdated = false;
}

void Arm::ResetScale() {
    aTotalModelScale = 1.f;
    aIsUpdated = false;
}

// 
void Arm::Reset() {
    ResetTranslation();
    ResetRotation();
    ResetScale();
    aElbowGroupMatrix = aDefaultElbowGroupMatrix;
    aElbowRotation = mat4(1.f);
    aWristGroupMatrix = aDefaultWristGroupMatrix;
    aWristRotation = mat4(1.f);
    // no need to set aIsUpdated to false since they already do it.
}
Racket * Arm::GetRacket(){
    return &aRacket;
}
vec3 Arm::GetCenterPosition() {
    return aShoulderGroupMatrix * aElbowGroupMatrix * aWristGroupMatrix * vec4(0, 0, 0, 1);
}