#ifndef SHADER_H
#define SHADER_H
#include <GL/glew.h>
#define INFO_LOG_SIZE 512
#include <cstdlib>
#include <glm/glm.hpp>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include "../common.h"
#include "../objects/directionalLight.h"
#include "../objects/pointLight.h"
#include "../objects/spotLight.h"
#include <vector>
using namespace glm;
using namespace std;

class Shader{
    public:

    Shader(const char * pVertexFileLocation, const char * pFragmentFileLocation);

    void CreateFromString(const char* vertexCode, const char* fragmentCode);
	void CreateFromFiles(const char* vertexLocation, const char* fragmentLocation);
    string ReadFile(const char* fileLocation);
    // 

    GLuint GetUniformLocation(const char * name);
    GLuint GetColorLocation();
    GLuint GetProjectionLocation();
	GLuint GetModelLocation();
	GLuint GetViewLocation();
    GLuint GetAmbientIntensityLocation();
    GLuint GetAmbientColorLocation();
    GLuint GetDiffuseIntensityLocation();
    GLuint GetDirectionLocation();
    GLuint GetSpecularIntensityLocation();
    GLuint GetShininessLocation();
    GLuint GetEyePositionLocation();
    
    void SetColor(vec3 pColor);
    void SetColor(vec4 pColor);
    void SetModelMatrix(mat4 pModelMatrix);
    void SetViewMatrix(mat4 pViewMatrix);
    void SetProjectionMatrix(mat4 pProjectionMatrix);
    void SetIsTextured(bool pIsTextured);
    void SetDirectionalLight(DirectionalLight * pDirectionalLight);
    void SetPointLights(vector<PointLight> & pPointLights);
    void SetSpotLights(vector<SpotLight> & pSpotLights);
    
    void SetTexture(GLuint pTextureUnit);
    void SetDirectionalShadowMap(GLuint pTextureUnit);
    void SetDirectionalLightTransform(GLfloat * plTransform);
    void SetDisplayShadows(bool pDisplayShadows);


    vec4 GetColor();
    mat4 GetModelMatrix();
    mat4 GetViewMatrix();
    mat4 GetProjectionMatrix();
    GLboolean GetIsTextured();
    GLboolean GetDisplayShadows();


    void SetEyePosition(vec3 pPos);


	void UseShader();
	void ClearShader();

	~Shader();

    protected:
    void CompileShader(const char* vertexCode, const char* fragmentCode);
	void AddShader(GLuint theProgram, const char* shaderCode, GLenum shaderType);

    GLuint aShaderProgramId;
    // locations
    GLuint aColorLocation;
    GLuint aModelMatrixLocation;
    GLuint aViewMatrixLocation;
    GLuint aProjectionMatrixLocation;
    GLuint aIsTexturedLocation;
    
    // specular
    GLuint aSpecularIntensityLocation;
    GLuint aShininessLocation;

    GLuint aEyePositionLocation;

    // directional light 
    struct {
        // ambient
        GLuint aAmbientColorLocation;
        GLuint aAmbientIntensityLocation;
        // diffuse
        GLuint aDiffuseIntensityLocation;
        GLuint aDirectionLocation;
    } aDirectionalLightLocation;
    
    uint aPointLightCount;
    GLuint aPointLightCountLocation; 

    uint aSpotLightCount;
    GLuint aSpotLightCountLocation;


    struct PointLightLocation{
        // ambient
        GLuint ambientColorLocation;
        GLuint ambientIntensityLocation;
        // diffuse
        GLuint diffuseIntensityLocation;
        GLuint positionLocation;
        GLuint constantLocation;
        GLuint linearLocation;
        GLuint exponentLocation;
    };
    PointLightLocation aPointLightLocations[MAX_POINT_LIGHTS];

    struct SpotLightLocation {
        PointLightLocation baseLocation;
        GLuint directionLocation;
        GLuint edgeLocation;
    } aSpotLightLocations[MAX_SPOT_LIGHTS];
    
    // shadow mapping
    GLuint aDirectionalLightTransformLocation, aDirectionalShadowMapLocation;
    GLuint aTextureLocation;
    GLuint aDisplayShadowsLocation; 
    // values of locations
    vec4 aColor;
    mat4 aModelMatrix;
    mat4 aViewMatrix;
    mat4 aProjectionMatrix;
    GLboolean aIsTextured;
    GLboolean aDisplayShadows;

    void SetUniformMatrix4fv(GLuint pLocation, GLfloat * pMatrix);
    void SetUniform3fv(GLuint pLocation, vec3 & pVector);
    void SetUniform4fv(GLuint pLocation, vec4 & pVector);
    void SetUniform1i(GLuint pLocation, GLint pInt);

    private:
};
#endif

