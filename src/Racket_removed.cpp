#include "Racket_removed.h"
#include "common.h"
GLuint Racket::cubeEBOLength = 36;
GLuint Racket::cubeEBODrawMethod = GL_TRIANGLES;

Racket::Racket(GLuint pVAOCubeId, GLuint pCubeEBO, GLuint pColorLocation, GLuint pModelMatrixLocation, vec3 pColor) {
    // when multipliying this matrix it will place the elbow ready for rotation.
    VAOCubeId = pVAOCubeId;
    cubeEBO = pCubeEBO;
    colorLocation = pColorLocation;
    modelMatrixLocation = pModelMatrixLocation;
    color = pColor;
    wasdTranslation = mat4(1.0);

    mat4 cubeOnGround = translate(mat4(1.0f), 0.5f * Y_DIR);
    vec3 racketBlockDims(0.15f*U, 1.f*U, 0.15f*U);
    wristGroupMatrix = translate(mat4(1.0), 2.f*U*Y_DIR);
    // create a new block and add 0.3f to the height of the block to 
    vec3 racketHandleDims = racketBlockDims + vec3(0.f, .1f, 0.f);
    racketHandle = scale(mat4(1.0), racketHandleDims) * cubeOnGround;

    // scale the stick to be sitting on xz plane and length vertically.
    // create a bottom rim with racket handle dimensions. 
    // bottom face facing the ground
    // and x and z centered at origin. 
    GLfloat rimThickness = racketHandleDims.z*0.5;
    vec3 racketTopBotRimDims = vec3(racketHandleDims.y*0.9, rimThickness, rimThickness);
    mat4 shapeRacketBottomRim = scale(mat4(1.0), racketTopBotRimDims);
    mat4 transformRacketBottomRimGroup = translate(mat4(1.0), racketHandleDims.y * U * Y_DIR);
    racketBottomRim =  transformRacketBottomRimGroup * shapeRacketBottomRim * cubeOnGround;

    vec3 sideRimDims = vec3(rimThickness, racketHandleDims.y * 1.5f, rimThickness);
    mat4 scaledSideRim = scale(mat4(1.0), sideRimDims) * cubeOnGround;
    vec3 posXSideRimTranslation = (racketTopBotRimDims.x + sideRimDims.x)/2.f *U*X_DIR;
    // negative x translation.
    mat4 racketLeftRimGroup = translate(transformRacketBottomRimGroup, -posXSideRimTranslation);
    racketLeftRim = racketLeftRimGroup * scaledSideRim;
    
    // right racket rim.
    // positive x translation
    racketRightRim = translate(transformRacketBottomRimGroup, posXSideRimTranslation) * scaledSideRim;
    mat4 bottomRimTranslate = translate(mat4(1.0), (sideRimDims.y - racketTopBotRimDims.y)*U*Y_DIR);
    lastTransformMatrixGroup = translate(mat4(1.0), racketTopBotRimDims.y*U*Y_DIR) * bottomRimTranslate * transformRacketBottomRimGroup;
    racketTopRim =  bottomRimTranslate * racketBottomRim;

    GLfloat meshHeight = sideRimDims.y - 2 * racketTopBotRimDims.y;
    GLfloat meshWidth = racketTopBotRimDims.x;
    // make the spacing between strings 10% of the mesh width
    GLfloat spacingBetweenStrings = 0.10 * meshWidth;
    // make the mesh strings width to be half of the spacing between them.
    GLfloat stringWidth = 0.08 * spacingBetweenStrings;
    vec3 verticalStringDims(stringWidth, meshHeight, stringWidth);
    vec3 horizontalStringDims(meshWidth, stringWidth, stringWidth);
    
    // don't create first and last one on the horizontal axis
    vec3 stringPos(
        (-meshWidth/2 + spacingBetweenStrings)*U, // x
        racketTopBotRimDims.y*U,                  // y
        0.f);                                     // z;

    mat4 stringMatrix = scale(mat4(1.0), verticalStringDims) * cubeOnGround;
    for (; stringPos.x < meshWidth/2.f; stringPos.x += spacingBetweenStrings + verticalStringDims.x) {
        mat4 m = 
            transformRacketBottomRimGroup *
            translate(mat4(1.0), stringPos) *
            stringMatrix;
        meshMatrices.push_back(m);
    }
    stringPos.y = racketTopBotRimDims.y + spacingBetweenStrings;
    stringPos.x = 0;
    stringMatrix = scale(mat4(1.0), horizontalStringDims) * cubeOnGround;
    for (; stringPos.y < meshHeight + racketTopBotRimDims.y; stringPos.y += spacingBetweenStrings + horizontalStringDims.y) {
        mat4 m = 
            transformRacketBottomRimGroup *
            translate(mat4(1.0), stringPos) *
            stringMatrix;
        meshMatrices.push_back(m);
    }
    // initialize a letter matrix to nothing
    letters = {};
}

void Racket::draw() {
    glBindVertexArray(VAOCubeId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeEBO);
    glUniform3fv(colorLocation, 1, &(color[0]));

    // draw stick of racket
    glUniform3fv(colorLocation, 1, &(color[0]));
    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &(( wristGroupMatrix * racketHandle)[0][0]));
    glDrawElements(cubeEBODrawMethod, cubeEBOLength, GL_UNSIGNED_INT, 0);

    // draw single diagonal on stick
    glUniform3fv(colorLocation, 1, &(vec3(1.f, 1.f, 1.f)[0]));
    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &(( wristGroupMatrix * racketBottomRim)[0][0]));
    glDrawElements(cubeEBODrawMethod, cubeEBOLength, GL_UNSIGNED_INT, 0);

    // draw left rim
    glUniform3fv(colorLocation, 1, &(color[0]));
    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &((wristGroupMatrix * racketLeftRim)[0][0]));
    glDrawElements(cubeEBODrawMethod, cubeEBOLength, GL_UNSIGNED_INT, 0);

    // draw right rim
    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &((wristGroupMatrix * racketRightRim)[0][0]));
    glDrawElements(cubeEBODrawMethod, cubeEBOLength, GL_UNSIGNED_INT, 0);

    // draw top rim
    glUniform3fv(colorLocation, 1, &(vec3(1.f, 1.f, 1.f)[0]));
    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &((wristGroupMatrix * racketTopRim)[0][0]));
    glDrawElements(cubeEBODrawMethod, cubeEBOLength, GL_UNSIGNED_INT, 0);

    glUniform3fv(colorLocation, 1, &(vec3(0.f, 1.f, 0.f)[0]));
    for (int i = 0; i<meshMatrices.size(); i++) {
        glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &((wristGroupMatrix * meshMatrices[i])[0][0]));
        glDrawElements(cubeEBODrawMethod, cubeEBOLength, GL_UNSIGNED_INT, 0);
    }
    mat4 upTranslate = lastTransformMatrixGroup * translate(mat4(1.0), ((float)U/3.f)*Y_DIR);
    
    // middle letter
    for (int i = 0; i<letters.size(); i++) {
        letters[i].SetBaseGroupMatrix(wristGroupMatrix * upTranslate);
        letters[i].SetColor(0.7f*color);
        // letters[i].Draw();
    }

    // back letter
    for (int i = 0; i<letters.size(); i++) {
        letters[i].SetBaseGroupMatrix(
            wristGroupMatrix * 
            translate(mat4(1.0), -2.f *letters[i].GetWidthDepth().y*Z_DIR) *
            upTranslate);
        letters[i].SetColor(0.2f*color);
        // letters[i].draw();
    }
    
    // front letter
    for (int i = 0; i<letters.size(); i++) {
        letters[i].SetBaseGroupMatrix(
            wristGroupMatrix * 
            translate(mat4(1.0), 2.f*letters[i].GetWidthDepth().y*Z_DIR) *
            upTranslate);
        letters[i].SetColor(color);
        // letters[i].draw();
    }

}

void Racket::addLetter(Letter pL) {
    letters.push_back(pL);
}

void Racket::translateModel(vec3 distanceVec) {
    mat4 unitTranslation = translate(mat4(1.f), distanceVec);
    wasdTranslation = unitTranslation * wasdTranslation;
    wristGroupMatrix = unitTranslation * wristGroupMatrix;
}

void Racket::rotateModel(GLfloat rads, vec3 axis) {
    mat4 addedRotation = rotate(mat4(1.0f), rads, axis);
    modelRotation = addedRotation * modelRotation;
    wristGroupMatrix = wasdTranslation * addedRotation * inverse(wasdTranslation) * wristGroupMatrix;
}

vec3 Racket::getCenterPosition() {
    vec4 centerPosition = lastTransformMatrixGroup * vec4(0.f, 0.f, 0.f, 1.f);
    centerPosition = translate(mat4(1.0), -centerPosition.y/2.f*Y_DIR) * centerPosition;
    return wristGroupMatrix * centerPosition;
}