#ifndef RACKET_H
#define RACKET_H
#include <GL/glew.h>    // Include GLEW - OpenGL Extension Wrangler
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>  // GLM is an optimized math library with syntax to similar to OpenGL Shading Language
#include <glm/gtc/matrix_transform.hpp> // include this to create transformation matrices
#include <glm/common.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include "objects/letter.h"
using namespace glm; 
using namespace std;
class Racket {
    public:
    Racket(GLuint pVAOCubeId, GLuint pCubeEBO, GLuint pColorLocation, GLuint pModelMatrixLocation, vec3 pColor);
    // value of the number of indices for a GL_TRIANGLES
    static GLuint cubeEBOLength;
    static GLuint cubeEBODrawMethod;
    void draw();
    void translateModel(vec3 direction);
    void rotateModel(GLfloat rads, vec3 axis);
    void addLetter(Letter letter);
    vec3 getCenterPosition();
    // void setVAOCubeId(GLuint VAOCubeId);
    private:
    GLuint VAOCubeId;
    GLuint colorLocation;   
    GLuint modelMatrixLocation;
    vec3 color;
    GLuint cubeEBO;
    mat4 wasdTranslation;
    mat4 modelRotation;
    mat4 wristGroupMatrix;
    mat4 racketHandle;
    mat4 racketBottomRim;
    mat4 racketLeftRim;
    mat4 racketRightRim;
    mat4 racketTopRim;
    mat4 lastTransformMatrixGroup;
    vector<mat4> meshMatrices;
    vector<Letter> letters;
    
};
#endif