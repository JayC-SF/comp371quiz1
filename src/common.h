#ifndef COMMON_H
#define COMMON_H
// COMP 371 Labs Framework
#include <cstdlib>
#pragma GCC optimize("O2")

// #include <cstdlib>
#include <iostream>


#define GLEW_STATIC 1   // This allows linking with Static Library on Windows, without DLL
#include <GL/glew.h>    // Include GLEW - OpenGL Extension Wrangler

#include <GLFW/glfw3.h> // GLFW provides a cross-platform interface for creating a graphical context,
                        // initializing OpenGL and binding inputs
#include <glm/glm.hpp>  // GLM is an optimized math library with syntax to similar to OpenGL Shading Language
#include <glm/gtc/matrix_transform.hpp> // include this to create transformation matrices
#include <glm/common.hpp>
#include <vector>
#include <glm/gtc/type_ptr.hpp>
#include <random>

using namespace glm;
using namespace std;
#define INFO_LOG_SIZE 512
#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768
#define ASPECT_RATIO ((float)WINDOW_WIDTH)/((float)WINDOW_HEIGHT)
#define U 1
#define X_DIR vec3(1.f, 0, 0)
#define Y_DIR vec3(0, 1.f, 0)
#define Z_DIR vec3(0, 0, 1.f)
#define GRID_LENGTH 78
#define GRID_WIDTH 36
#define GRID_SIZE 100

#define MODEL_SCALING_FACTOR 0.01
#define CLOSEUP_RADIUS 8.f
#define FAR_RADIUS 25.f
#define WASD_SPEED 2.*U
#define CLOSEUP_SPEED 1.*U
#define WORLD_ANGULAR_SPEED 5.f //degrees
#define PANNING_ANGULAR_SPEED 2.f //degrees
#define ZOOM_SPEED 20.f //degrees
#define GLM_FORCE_RADIANS
#define NEAR_PLANE 0.01
#define FAR_PLANE 1000.f
#define MAX_POINT_LIGHTS 3
#define MAX_SPOT_LIGHTS 4

typedef struct {
    GLuint key;
    GLuint state;
} last_state;
#endif 