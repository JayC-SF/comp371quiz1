#include "common.h"
#include "shaders/include.h"
#include "objects/include.h"
#define RACKET_BLUE 0
#define RACKET_ORANGE 1

float randomFloat(float min, float max) {
    
    double randomNum = ((double)rand())/RAND_MAX;
    randomNum *= (max - min);
    randomNum += min;
    return randomNum;
}

int main(int argc,  char * argv[]) {
    
    // Init the glfw library and set various configurations.
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create a glfw window with specified size
    // glfwCreateWindow(width, size, title, monitor, share)
    GLFWwindow * window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Tennis Racket Assignment 1", NULL, NULL);
    // If cannot create window we abort the program.
    if (!window) {
        std::cerr << "Failed to open GLFW Window." << std::endl;
        exit(EXIT_FAILURE);
    }
    // Set context current context to this window.
    glfwMakeContextCurrent(window);
    // disable the mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    // Initialize GLEW
    // Set experimental features needed for core profile
    glewExperimental = true;
    // init glew and check if it was properly init.
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to init GLEW" << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Bind a VAO before creating a shader, this will prevent a validation error. 
    Cube * cube = Cube::GetInstance();
    cube->BindAttributes();
    // Create our main shader
    Shader shader = Shader("../assets/shaders/vertexShader.glsl", "../assets/shaders/fragmentShader.glsl");
    shader.UseShader();
    
    DirectionalLight mainLight = DirectionalLight(2048*2, 2048*2, vec3(1.f, 1.f, 1.f), 1.f, 
        vec3(.0f, -5.f, -10.f), 0.f);
    shader.SetDirectionalLight(&mainLight);
    shader.SetIsTextured(false);
    
    // ground for the tennis court
    Ground ground(GRID_WIDTH, GRID_LENGTH, vec3(0.f, 1.f, 1.f));


    // glClearColor(red, green, blue, alpha);
    glClearColor(37.f/255., 30.f/255., 62.f/255., 1);


    // Set the viewport to avoid the jittering in the beginning.
    GLint frameBufferWidth, frameBufferHeight;
    glfwGetFramebufferSize(window, &frameBufferWidth, &frameBufferHeight);
    glViewport(0, 0, frameBufferWidth, frameBufferHeight);

    // have the brachium and more to be rotated by 30 degrees.
    float cameraHorizontalAngle = 90;
    float cameraVerticalAngle = -30;
    float theta = radians(cameraHorizontalAngle);
    float phi = radians(cameraVerticalAngle);
    vec3 cameraLookAt = vec3(cosf(phi)*cosf(theta), sinf(phi), -cosf(phi)*sinf(theta));
    vec3 cameraPosition = vec3(0);
    
    vec3 cameraUp = vec3(0.f*U, 1.0f*U, 0.f*U);
    GLfloat cameraRadius = FAR_RADIUS;
    vec3 position = cameraPosition - cameraRadius * cameraLookAt;
    mat4 viewMatrix = lookAt(position, cameraPosition + cameraLookAt, cameraUp);
    shader.SetViewMatrix(viewMatrix);

    // set projection matrix to perspective view.
    GLfloat fovy = radians(60.f);
    mat4 perspectiveMatrix = perspective(fovy, ASPECT_RATIO, 0.01f*U, 1000.0f*U);
    shader.SetProjectionMatrix(perspectiveMatrix);

    // 1 unit a second.
    GLfloat lastFrame = glfwGetTime();
    // keep the last mouse state at all times.
    GLdouble lastMousePosX, lastMousePosY;
    glfwGetCursorPos(window, &lastMousePosX, &lastMousePosY);
    // Enables the Depth Buffer
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    
    int initialPosZ = (GRID_LENGTH/4)*U;
    vector<Arm> arms = {
        Arm(),
        Arm(),
    };

    // Create four rackets
    LetterS letterS (vec3(0, 0, 1));
    LetterR letterR (vec3(1, 0, 0));
    letterS.Translate(-X_DIR);
    letterR.Translate(X_DIR);
    arms[RACKET_BLUE].Translate(vec3(0, 0, initialPosZ));
    arms[RACKET_BLUE].GetRacket()->SetColor(vec3(0, 0, 1)); //blue
    arms[RACKET_BLUE].GetRacket()->AddLetter(letterS);
    arms[RACKET_BLUE].GetRacket()->AddLetter(letterR);
    
    LetterE letterE (vec3(1.f, 165.f/255.f, 0));
    LetterN letterN (vec3(0.5f, 0, 0.5f));
    letterE.Translate(-X_DIR);
    letterN.Translate(X_DIR);
    arms[RACKET_ORANGE].Translate(vec3(0, 0, -initialPosZ));
    arms[RACKET_ORANGE].GetRacket()->SetColor(vec3(1.f, 165.f/255.f, 0)), //orange
    arms[RACKET_ORANGE].GetRacket()->AddLetter(letterE);
    arms[RACKET_ORANGE].GetRacket()->AddLetter(letterN);

    TennisNet tennisNet(vec3(0.5f));
    Arm * currentArmPtr = NULL;
    Sky sky = Sky(100, vec3(115.f/255.f, 186.f/255.f, 215.f/255.f));

    while(!glfwWindowShouldClose(window)) {
        GLfloat dt = glfwGetTime() - lastFrame;
        lastFrame += dt;
        GLdouble mousePosX, mousePosY;
        glfwGetCursorPos(window, &mousePosX, &mousePosY);
        
        // set the dx and dy for the mouse.
        double dx = mousePosX - lastMousePosX, dy = mousePosY - lastMousePosY;
        lastMousePosX = mousePosX;
        lastMousePosY = mousePosY;
        
        // clear current buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // draw all the rackets.
        shader.UseShader();
       
        ground.Draw(&shader);
        tennisNet.Draw(&shader);

        // draw sky box
        sky.Draw(&shader);
        
        for (int i = 0; i<arms.size(); i++) {
            arms[i].Draw(&shader);
        }

        glfwSwapBuffers(window);

        // handle the camera view and position
        cameraHorizontalAngle -= dx * PANNING_ANGULAR_SPEED * dt;
        cameraVerticalAngle  -= dy * PANNING_ANGULAR_SPEED * dt;
        cameraVerticalAngle = std::max(-85.0f, std::min(85.0f, cameraVerticalAngle));

        if (cameraHorizontalAngle > 360)
            cameraHorizontalAngle -= 360;
        else if (cameraHorizontalAngle < -360)
            cameraHorizontalAngle += 360;
        
        float theta = radians(cameraHorizontalAngle);
        float phi = radians(cameraVerticalAngle);
        
        cameraLookAt = vec3(cosf(phi)*cosf(theta), sinf(phi), -cosf(phi)*sinf(theta));
        // viewMatrix = inverse(currentWorldRotation) * inverse(currentWorldPanning) * viewMatrix;
        position = cameraPosition - cameraRadius * cameraLookAt;
        viewMatrix = lookAt(position, cameraPosition + cameraLookAt, cameraUp);
        // viewMatrix = currentWorldPanning * currentWorldRotation * viewMatrix; 
        shader.SetViewMatrix(viewMatrix);

        if (currentArmPtr)
            cameraPosition = currentArmPtr->GetCenterPosition();

        // handle events
        glfwPollEvents();
        
        // Upon pressing escape, stop window, and terminate program later.
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, true);
        
        bool shiftPressed = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS; 
        shiftPressed = shiftPressed || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS;

        // reset point of focus to the center of the grid
        if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS) {
            currentArmPtr = NULL;
            cameraPosition = vec3(0);
            cameraRadius = FAR_RADIUS;
        }
        // set point of focus on the blue tennis racket.
        if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
            currentArmPtr = &arms[RACKET_BLUE];
            cameraPosition = currentArmPtr->GetCenterPosition();
            cameraRadius = CLOSEUP_RADIUS;
        }
        // set point of focus on the red tennis racket.
        if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
            currentArmPtr = &arms[RACKET_ORANGE];
            cameraPosition = currentArmPtr->GetCenterPosition();
            cameraRadius = CLOSEUP_RADIUS;
        }
        // set point of focus on the orange tennis racket.
        // if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
        //     currentArmPtr = &arms[RACKET_ORANGE];
        //     cameraPosition = currentArmPtr->GetCenterPosition();
        //     cameraRadius = CLOSEUP_RADIUS;
        // }
        // set point of focus on the purple tennis racket.
        // if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS) {
        //     currentArmPtr = &arms[RACKET_PURPLE];
        //     cameraPosition = currentArmPtr->GetCenterPosition();
        //     cameraRadius = CLOSEUP_RADIUS;
        // }
        if (currentArmPtr != NULL && glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS && shiftPressed) {
            // unit translation to the left
            currentArmPtr->Translate(-(float)WASD_SPEED * U * dt*X_DIR);
        }
        // rotate the current model.
        else if (currentArmPtr != NULL && glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            currentArmPtr->Rotate(-radians<float>(5), Y_DIR);
        }

        if (currentArmPtr != NULL &&glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && shiftPressed)
            // unit translation to the left
            currentArmPtr->Translate((float)WASD_SPEED * U * dt*X_DIR);

        else if (currentArmPtr != NULL &&glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
            currentArmPtr->Rotate(radians<float>(5), vec3(0.f, 1.f, 0.f));

        if (currentArmPtr != NULL &&glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS && shiftPressed)
            // unit translation to the left
            currentArmPtr->Translate((float)WASD_SPEED * dt*U * Y_DIR);

        if (currentArmPtr != NULL &&glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && shiftPressed)
            // unit translation to the left
            currentArmPtr->Translate((float)-WASD_SPEED * dt*U*Y_DIR);

        if (currentArmPtr != NULL && glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS && shiftPressed)
            // unit translation to the left
            currentArmPtr->Translate((float)-WASD_SPEED * dt*U*Z_DIR);

        if (currentArmPtr != NULL &&glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS && shiftPressed)
            // unit translation to the left
            currentArmPtr->Translate((float)WASD_SPEED * dt*U*Z_DIR);
        
    }
    glDisable(GL_BLEND);
    // cleanup
    // Terminate the program.
    glfwTerminate();
}