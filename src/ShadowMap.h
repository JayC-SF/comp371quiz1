#ifndef SHADOWMAP_H
#define SHADOWMAP_H

#include <stdio.h>
#include <GL/glew.h>

class ShadowMap {
    public:
    ShadowMap();
    ~ShadowMap();

    virtual bool Init(GLuint pWidth, GLuint pHeight);
    virtual void Write();
    virtual void Read(GLenum pTextureUnit);
    
    GLuint GetShadowWidth() {return aShadowWidth;}
    GLuint GetShadowHeight() {return aShadowHeight;}
    

    protected:
    GLuint aFBO, aShadowMapId;
    GLuint aShadowWidth, aShadowHeight;


};
#endif